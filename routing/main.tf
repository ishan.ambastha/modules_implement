## Route Table Public
resource "aws_route_table" "public_rt" {
  vpc_id = var.vpc_id ##--

  route {
    cidr_block = var.igw_cidr
    gateway_id = var.igw_gateway ##--
  }

  tags = {
    Name = var.pub_route_table
  }
}

# # Route table associatiolln with public subnets
# resource "aws_route_table_association" "public" {
#   count          = length(var.subnet_pub)
#   subnet_id      = aws_subnet.squid_pubsub[count.index].id
#   route_table_id = aws_route_table.public_rt.id
# }
