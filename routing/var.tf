variable "pub_route_table" {
  description = "Public Route Table name"
  default     = "squid-route-pub-01/02"
}

variable "igw_cidr" {
  description = "Internet gateway cidr value"
  default     = "0.0.0.0/0"
}

variable "subnet_pub" {
  description = "subnets cidr values"
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "vpc_id" {

}

variable "igw_gateway" {

}