variable "AMI" {
  description = "AMI of region"
  default     = "ami-0892d3c7ee96c0bf7"
}

variable "instance_type" {
  description = "Instance Type"
  default     = "t2.micro"
}

variable "security" {

}

variable "subnet_id" {

}