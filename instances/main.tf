##Squid_proxy-instance -> Public instance
resource "aws_instance" "ubuntu" {
  ami                         = var.AMI
  instance_type               = var.instance_type
  associate_public_ip_address = "true"
  user_data                   = <<-EOF
                    #!/bin/bash
                    sudo apt-get update -y
                    sudo apt-get install squid -y
                EOF

  # VPC
  subnet_id = var.subnet_id

  # Security Group
  vpc_security_group_ids = var.security

  tags = {
    Name = "Squid-instance"
  }
}
