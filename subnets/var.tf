variable "subnet_pub" {
  description = "cidr values of subnets"
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "subnet_zones" {
  description = "subnets availability zones"
  default     = ["us-west-2a", "us-west-2b"]
}

variable "pub_sub_name" {
  description = "Tag names of subnets"
  default     = ["sub-01", "sub-02"]
}

variable "vpc_id" {
  description = "squid vpc id"
}