## Subnets

# Public Subnets
resource "aws_subnet" "squid_pubsub" {
  count             = length(var.subnet_pub)
  vpc_id            = var.vpc_id
  cidr_block        = element(var.subnet_pub, count.index)
  availability_zone = element(var.subnet_zones, count.index)

  tags = {
    Name = var.pub_sub_name[count.index]
  }
}
