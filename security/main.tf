##Security group
 locals {
  ports = [0, 22]
}
resource "aws_security_group" "squid_security_group" {
  vpc_id = var.vpc_id

  dynamic "ingress" {
    for_each = local.ports
    content {
      description = "description ${ingress.key}"
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  tags = {
    Name = "Ins2-sg-grp"
  }
}
