output "squid_security_group" {
  description = "Security group"
  value       = aws_security_group.squid_security_group
}