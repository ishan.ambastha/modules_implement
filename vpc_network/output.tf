output "squid_vpc" {
  description = "Output value of squid vpc"
  value       = aws_vpc.squid_vpc.id
}

output "squid_igw" {
  description = "utput value of squid internet gateway"
  value       = aws_internet_gateway.squid_igw.id
}
