# Default variable file

variable "vpc_name" {
  type        = string
  description = "Name of the vpc"
  default     = "Squid-Proxy"
}

variable "igw_name" {
  type        = string
  description = "Name of the Internet Gateway"
  default     = "Squid-igw"
}

variable "nacl_name" {
  type        = string
  description = "Name of the Nacl group"
  default     = "Squid-nacl"
}

