#VPC Module
terraform {
  required_version = ">=1.0"
}

resource "aws_vpc" "squid_vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = var.vpc_name
  }
}

## Internet Gateway
resource "aws_internet_gateway" "squid_igw" {
  vpc_id = aws_vpc.squid_vpc.id

  tags = {
    Name = var.igw_name
  }
}

## Nacl group
resource "aws_network_acl" "squid-network-acl" {
  vpc_id = aws_vpc.squid_vpc.id

  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = var.nacl_name
  }
}
